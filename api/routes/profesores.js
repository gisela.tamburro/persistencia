var express = require("express");
var router = express.Router();
var models = require("../models");

router.get("/", (req, res) => {

  models.Profesores.findAll({
      attributes: ["id","apellido" ,"nombre", "mail"],
       include: ["materia"] 
    })
    .then(profesores => res.send(profesores))
    .catch(() => res.sendStatus(500));
});

// router.getMaterias("/", (req, res) => {

//   models.Profesores.findAll({
//       attributes: ["id","apellido" ,"nombre", "mail"],
//       include:[{as:'cantidad-de-materias', model:models.materia, attributes: ["id","nombre"]}],

//     })
//     .then(profesores => res.send(profesores))
//     .catch(() => res.sendStatus(500));
// });


// const cantidadDeMaterias =() =>{
//  models.profesores.findAll({attributes: ["id_materia"]}),
// };

router.post("/", (req, res) => {
  models.Profesores
    .create({apellido:req.body.apellido,nombre:req.body.nombre,mail:req.body.mail})
    .then(profesores => res.status(201).send({ id: profesores.id }))
    .catch(error => {
      if (error == "SequelizeUniqueConstraintError: Validation error") {
        res.status(400).send('Bad request: existe otro profesor con el mismo nombre')
      }
      else {
        console.log(`Error al intentar insertar en la base de datos: ${error}`)
        res.sendStatus(500)
      }
    });
});

const findProfesor = (id, { onSuccess, onNotFound, onError }) => {
  models.Profesores
    .findOne({
      attributes: ["id","apellido" ,"nombre", "mail"],
      where: { id }
    })
    .then(profesores => (profesores ? onSuccess(profesores) : onNotFound()))
    .catch(() => onError());
};

router.get("/:id", (req, res) => {
  findProfesor(req.params.id, {
    onSuccess: profesores => res.send(profesores),
    onNotFound: () => res.sendStatus(404),
    onError: () => res.sendStatus(500)
  });
});

router.put("/:id", (req, res) => {
  const onSuccess = profesores =>
  profesores
      .update({ apellido: req.body.apellido, nombre: req.body.nombre , mail: req.body.mail }, { fields: ["apellido" ,"nombre", "mail"] })
      .then(() => res.sendStatus(200))
      .catch(error => {
        if (error == "SequelizeUniqueConstraintError: Validation error") {
          res.status(400).send('Bad request: existe otrO profesor con el mismo nombre')
        }
        else {
          console.log(`Error al intentar actualizar la base de datos: ${error}`)
          res.sendStatus(500)
        }
      });
    findProfesor(req.params.id, {
    onSuccess,
    onNotFound: () => res.sendStatus(404),
    onError: () => res.sendStatus(500)
  });
});

router.delete("/:id", (req, res) => {
  const onSuccess = profesor =>
  profesor
      .destroy()
      .then(() => res.sendStatus(200))
      .catch(() => res.sendStatus(500));
  findProfesor(req.params.id, {
    onSuccess,
    onNotFound: () => res.sendStatus(404),
    onError: () => res.sendStatus(500)
  });
});

module.exports = router;
