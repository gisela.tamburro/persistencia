'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profesores = sequelize.define('Profesores', {
    apellido: DataTypes.STRING,
    nombre: DataTypes.STRING,
    mail: DataTypes.STRING
  }, 
  {});
  const materia = sequelize.define('materia', {
    name: DataTypes.STRING
  }, {timestamps: false });
  Profesores.hasMany(materia,  
    {             
      foreignKey: 'id_profesor'       
    }) ; 
  return Profesores;
};