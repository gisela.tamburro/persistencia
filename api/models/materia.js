'use strict';

const { INTEGER } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  const materia = sequelize.define('materia', {
    name: DataTypes.STRING,
    id_carrera: DataTypes.INTEGER,
    id_profesor: DataTypes.INTEGER

  }, {});
  materia.associate = function(models) {
    
  	materia.belongsTo(models.carrera
    ,{
      as : 'carrera-Relacionada', 
      foreignKey: 'id_carrera'     
    });
    materia.belongsTo(models.Profesores
      ,{
        as : 'profesor-Relacionado', 
        foreignKey: 'id_profesor'     
      });
    

  };
 
  
 
  return materia;
};